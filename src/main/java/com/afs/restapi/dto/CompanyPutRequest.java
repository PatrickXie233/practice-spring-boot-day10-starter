package com.afs.restapi.dto;

public class CompanyPutRequest {
    private String name;

    public CompanyPutRequest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
