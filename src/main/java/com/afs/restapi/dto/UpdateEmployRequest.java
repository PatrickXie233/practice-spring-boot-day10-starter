package com.afs.restapi.dto;

public class UpdateEmployRequest {
    private Integer age;
    private Integer salary;

    public UpdateEmployRequest(Integer age, Integer salary) {
        this.age = age;
        this.salary = salary;
    }

    public UpdateEmployRequest() {
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }
}
