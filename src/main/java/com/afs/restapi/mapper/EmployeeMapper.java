package com.afs.restapi.mapper;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.dto.InsertEmployeeRequest;
import com.afs.restapi.dto.EmployeeResponse;
import org.springframework.beans.BeanUtils;

public class EmployeeMapper {
    public static Employee toEntityResponse(InsertEmployeeRequest insertEmployeeRequest){
        Employee employee = new Employee();
        BeanUtils.copyProperties(insertEmployeeRequest,employee);
        return employee;
    }
    public static EmployeeResponse toEntityResponse(Employee employee){
        EmployeeResponse employeeResponse = new EmployeeResponse();
        BeanUtils.copyProperties(employee,employeeResponse);
        return employeeResponse;
    }

}
