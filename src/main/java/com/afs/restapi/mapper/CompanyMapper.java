package com.afs.restapi.mapper;

import com.afs.restapi.entity.Company;
import com.afs.restapi.dto.CompanyPostRequest;
import org.springframework.beans.BeanUtils;

public class CompanyMapper {
    public static Company toCompany(CompanyPostRequest companyPostRequest) {
        Company company = new Company();
        BeanUtils.copyProperties(companyPostRequest, company);
        return company;
    }
}
