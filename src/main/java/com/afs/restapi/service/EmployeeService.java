package com.afs.restapi.service;

import com.afs.restapi.dto.UpdateEmployRequest;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.dto.InsertEmployeeRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.afs.restapi.mapper.EmployeeMapper.toEntityResponse;

@Service
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<EmployeeResponse> findAllByStatusTrue() {
        return employeeRepository.findAllByStatusTrue();
    }

    public Employee update(int id, UpdateEmployRequest updateEmployRequest) {
        Employee employee = employeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        if (updateEmployRequest.getAge() != null) {
            employee.setAge(updateEmployRequest.getAge());
        }
        if (updateEmployRequest.getSalary() != null) {
            employee.setSalary(updateEmployRequest.getSalary());
        }
        employeeRepository.save(employee);
        return employee;
    }

    public EmployeeResponse findById(int id) {
        return employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public List<EmployeeResponse> findByGender(String gender) {
        return employeeRepository.findByGenderAndStatusTrue(gender);
    }

    public List<EmployeeResponse> findByPage(int pageNumber, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        return employeeRepository.findAllByStatusTrue(pageRequest).toList();
    }

    public Employee insert(InsertEmployeeRequest insertEmployeeRequest) {
        return employeeRepository.save(toEntityResponse(insertEmployeeRequest));
    }

    public void delete(int id) {
        Employee employee = employeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        employee.setStatus(false);
        employeeRepository.save(employee);
    }
}
