package com.afs.restapi.repository;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.dto.EmployeeResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    @Query("SELECT new com.afs.restapi.dto.EmployeeResponse(e.name, e.age, e.gender, e.companyId) FROM Employee e WHERE e.gender = :gender AND e.status = true")
    List<EmployeeResponse> findByGenderAndStatusTrue(@Param("gender") String gender);

    @Query("SELECT new com.afs.restapi.dto.EmployeeResponse(e.name, e.age, e.gender, e.companyId) FROM Employee e WHERE e.status = true")
    List<EmployeeResponse> findAllByStatusTrue();

    @Query("SELECT new com.afs.restapi.dto.EmployeeResponse(e.name, e.age, e.gender, e.companyId) FROM Employee e WHERE e.status = true AND e.id = :id")
    Optional<EmployeeResponse> findByIdAndStatusTrue(@Param("id") Integer id);
    @Query("SELECT new com.afs.restapi.dto.EmployeeResponse(e.name, e.age, e.gender, e.companyId) FROM Employee e")
    Page<EmployeeResponse> findAllByStatusTrue(PageRequest pageRequest);

    List<Employee> findAllByCompanyId(Integer companyId);

    List<Employee> findAllByCompanyIdIn(List<Integer> companyIds);
}
