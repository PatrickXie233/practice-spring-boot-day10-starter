package com.afs.restapi;

import com.afs.restapi.dto.UpdateEmployRequest;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;

@ExtendWith(SpringExtension.class)
class EmployeeServiceTest {

    @Mock
    EmployeeRepository employeeRepository;

    @InjectMocks
    EmployeeService employeeService;

    @Test
    void should_return_all_employees_when_find_all_given_employees() {
        // given
        EmployeeResponse employeeResponse = new EmployeeResponse("Susan", 22, "Female", 100);
        ArrayList<EmployeeResponse> employees = new ArrayList<>();
        employees.add(employeeResponse);
        given(employeeRepository.findAllByStatusTrue()).willReturn(employees);

        // when
        List<EmployeeResponse> result = employeeService.findAllByStatusTrue();

        // should
        assertThat(result, hasSize(1));
        assertThat(result.get(0), equalTo(employeeResponse));
    }

    @Test
    void should_update_only_age_and_salary_when_update_given_employee() {
        // given
        String originalName = "Susan";
        Integer newAge = 23;
        String originalGender = "Female";
        Integer newSalary = 10000;
        Integer companyId = 100;
        Employee employee = new Employee(1,originalName,newAge,originalGender,newSalary,companyId);
        UpdateEmployRequest updateEmployRequest = new UpdateEmployRequest(newAge,newSalary);
        given(employeeRepository.findById(1)).willReturn(
                Optional.of(new Employee(1,originalName, 22, originalGender,10000, 100))
        );
        // when
        Employee updatedEmployee = employeeService.update(1, updateEmployRequest);

        // should
        assertThat(updatedEmployee.getName(), equalTo(originalName));
        assertThat(updatedEmployee.getAge(), equalTo(newAge));
        assertThat(updatedEmployee.getGender(), equalTo(originalGender));
        assertThat(updatedEmployee.getSalary(), equalTo(newSalary));
    }
}
