**O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?**

1. codeReview：存在一些code small 比如魔法值
2. spring boot mapper：学习并实践了springboot中的mapper层，其中dto数据传输对象主要用于对数据进行二次封装，满足各层之间数据传输需求
3. flyway：数据库版本管理工具，学习并实践了flyway的配置以及基础使用
4. 回顾会：我们在回顾会中总结了这两个星期做的好的，做的不够好的，建议以及困惑，并提出解决方案，我需要注意以下几个action：
   1. Teamwork方面：轮流组织teamwork，统一时间进行teamwork以及在codeReview中要挑重点
   2. 专注方面：上课要集中精神，在课上有困惑要及时问老师，小组成员要相互监督
5. session：我们组的主题是容器技术，我负责的是容器技术未来发展趋势，但我收集的资料不全，需要进行改进

**R (Reflective): Please use one word to express your feelings about today's class.**

开放

**I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?**

通过回顾会让我们对过去两周有个很好的总结，也让我们明白了接下来需要加强的方面

**D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?**

在teamwork中更加积极，在上课需要更加认真听讲

